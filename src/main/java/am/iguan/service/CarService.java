package am.iguan.service;

import am.iguan.entity.Car;

import java.util.List;

public interface CarService {

    Car save(Car car);

    Car findById(Long id);

    Car findByUserId(Long id);

    void delete(Car entity);

    List<Car> findAll();
}
