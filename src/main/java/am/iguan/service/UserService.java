package am.iguan.service;

import am.iguan.entity.User;

import java.util.List;

public interface UserService {

    User save(User user);

    List<User> findAll();

    User findById(Long userId);

    List<User> findWhereEmailIsGmail();

}
