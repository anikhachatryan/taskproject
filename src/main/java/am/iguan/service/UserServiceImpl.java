package am.iguan.service;

import am.iguan.entity.User;
import am.iguan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepository usersRepository;

    @Autowired
    public UserServiceImpl(UserRepository usersRepository){
        this.usersRepository = usersRepository;
    }

    public User save(User user) {
        return usersRepository.save(user);
    }

    public List<User> findAll(){
        return usersRepository.findAll();
    }

    public User findById(Long userId){
        return usersRepository.findById(userId).orElse(null);
    }

    public List<User> findWhereEmailIsGmail(){
        return usersRepository.findWhereEmailIsGmail();
    }

}
