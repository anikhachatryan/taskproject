package am.iguan.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Car implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String model;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_car")
    private User user;

    public Car() {
    }

    public Car(Long id, String model, User user) {
        this.id = id;
        this.model = model;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
