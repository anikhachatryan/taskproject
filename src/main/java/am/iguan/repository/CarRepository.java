package am.iguan.repository;

import am.iguan.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    Car save(Car car);

    Optional<Car> findById(Long id);

    void delete(Car entity);

    List<Car> findAll();

    Car findByUserId(Long id);
}
