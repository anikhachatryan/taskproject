package am.iguan.repository;

import am.iguan.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User save(User user);

    Optional<User> findById(Long id);

    @Query("select u from User u where u.email like '%@gmail.com%'")
    List<User> findWhereEmailIsGmail();

    List<User> findAll();

    void delete(User entity);
}