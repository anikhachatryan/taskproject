package am.iguan.controller;

import am.iguan.entity.Car;
import am.iguan.entity.User;
import am.iguan.service.CarService;
import am.iguan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class CarController {

    private final CarService carService;
    private final UserService userService;

    @Autowired
    public CarController(CarService carService, UserService userService) {
        this.carService = carService;
        this.userService = userService;
    }

    @GetMapping("/cars")
    public List<Car> getAll(){
        return carService.findAll();
    }

    @GetMapping("/cars/{id}")
    public ResponseEntity<Car> getById(@PathVariable long id) {
        Car byId = carService.findById(id);
        return ResponseEntity.ok(byId);
    }

    @GetMapping("/{id}/cars")
    public ResponseEntity<List<Car>> getCarsByUserId(@PathVariable long id) {
        User user = userService.findById(id);
        return ResponseEntity.ok(user.getCars());
    }

    @PostMapping("/cars")
    public ResponseEntity<Car> save(@RequestBody Car car) {
        Car saved = carService.save(car);
        return ResponseEntity.ok(saved);
    }

    @DeleteMapping("/cars/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        carService.delete(carService.findById(id));
        return ResponseEntity.ok().build();
    }
}
