package am.iguan.controller;

import am.iguan.entity.User;
import am.iguan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public List<User> getAll(){
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getById(@PathVariable long id) {
        User userById = userService.findById(id);
        return ResponseEntity.ok(userById);
    }

    @GetMapping("/gmails")
    public List<User> getAllWhereEmailIsGmail(){
        return userService.findWhereEmailIsGmail();
    }

    @PostMapping("/")
    public ResponseEntity<User> save(@RequestBody User user) {
        User saved = userService.save(user);
        return ResponseEntity.ok(saved);
    }



}
